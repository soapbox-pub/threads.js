// tslint:disable no-duplicate-imports
import type { BlobWorker as BlobWorkerClass } from "../types/master.js"
import { Worker as WorkerType } from "../types/master.js"
import { getWorkerImplementation, isWorkerRuntime } from "./implementation.js"

export { FunctionThread, ModuleThread } from "../types/master.js"
export { Pool } from "./pool.js"
export { spawn } from "./spawn.js"
export { Thread } from "./thread.js"
export { isWorkerRuntime }

export type BlobWorker = typeof BlobWorkerClass
export type Worker = WorkerType

/** Separate class to spawn workers from source code blobs or strings. */
export const BlobWorker = getWorkerImplementation().blob

/** Worker implementation. Either web worker or a node.js Worker class. */
export const Worker = getWorkerImplementation().default
