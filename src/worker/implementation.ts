// tslint:disable no-var-requires
/*
 * This file is only a stub to make './implementation' resolve to the right module.
 */

import WebWorkerImplementation from "./implementation.browser.js"

export default WebWorkerImplementation
